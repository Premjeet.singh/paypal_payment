from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import Product, Order

# Create your views here.


def paypalPayment(request):
	return render(request, 'base/paypal_payment.html')


def store(request):
	products = Product.objects.all()
	context = {'products':products}
	return render(request, 'base/store.html', context)


def checkout_payment(request, pk):
	product = Product.objects.get(id=pk)
	context = {'product':product}
	return render(request, 'base/checkout_payments.html', context)


def paymentComplete(request):
	body = json.loads(request.body)
	print('BODY:', body)
	product = Product.objects.get(id=body['productId'])
	order=Order.objects.create(
		product=product
		)
	context = {'order':order }
	return render(request, 'base/payment_done.html', context)