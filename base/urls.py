from django.urls import path
from . import views

urlpatterns = [
    path('paypal_payment/', views.paypalPayment, name="paypal_payment"),
    path('', views.store, name="store"),
    path('checkout_payment/<int:pk>/', views.checkout_payment, name="checkout_payment"),
    path('payment_complete/', views.paymentComplete, name="payment_complete"),
]