### What about project ?

Paypal Integration


### Project settings and configurations :

1. How do I run this project locally?
    1. Clone the repository:

2. $ cd django-paypal-master/

3. Create a virtual environment to install dependencies in and activate it:
    1. $ virtualenv paypal_env
    2. $ source env/bin/activate
    3.Then install the dependencies:
   (venv)$ pip install -r requirements.txt Note : the (env) in front of the prompt. This indicates that this terminal
   session operates in a virtual environment set up by virtualenv2.
   4. Once pip has finished downloading the dependencies:
    (venv)$ cd project
    (venv)$ python manage.py migrate
    (venv)$ python manage.py runserver

4. Run migrations:
    1. $ python manage.py migrate

5 Create a user:
$ python manage.py createsuperuser

6. Run the server:
   $ python manage.py runserver
   

7. And open 127.0.0.1:8000/in your web browser.
